# /*
#  * Escribe un programa que muestre por consola (con un print) los
#  * números de 1 a 100 (ambos incluidos y con un salto de línea entre
#  * cada impresión), sustituyendo los siguientes:
#  * - Múltiplos de 3 por la palabra "fizz".
#  * - Múltiplos de 5 por la palabra "buzz".
#  * - Múltiplos de 3 y de 5 a la vez por la palabra "fizzbuzz".
#  */

def fizzBuzz():
    for nu in range(1, 101):
         if(nu % 3 == 0 and nu % 5 == 0):
            print("fizzbuzz")
         elif (nu % 3 == 0):
            print("fizz")
         elif(nu % 5 == 0):
            print("buzz")
         else:
            print(nu)
fizzBuzz()
    
  